import { QuestionGroup } from '~/types/QuestionGroup'

export type TestTemplate = {
  id: number
  name: string
  authorId: number
  groups?: QuestionGroup[]
}

// export abstract class Question {
//   content: string
//   options
// }
