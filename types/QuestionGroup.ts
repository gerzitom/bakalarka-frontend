import { TestQuestion } from '~/types/Test'

export type QuestionGroup = {
  id: number
  name: string
  points: number
  questions: TestQuestion[]
}
