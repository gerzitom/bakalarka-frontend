/**
 * Duration in minutes
 */
export type Test = {
  id: number
  name: string
  startDate: Date
  duration: number
}

export abstract class Answers {
  abstract get answersTypeName():string
  abstract getInitialAnswers():any
}

export type UserAnswer = {
  questionId: number
  answer: number
}

export type OptionAnswer = {
  key: number
  value: string
}

export class OneOptionAnswers extends Answers {
  answers: OptionAnswer[]
  rightAnswer: number

  get answersTypeName(): string {
    return 'Výběr odpovědí s jednou správnou možností';
  }


  constructor(answers: OptionAnswer[], rightAnswer: number) {
    super()
    this.answers = answers
    this.rightAnswer = rightAnswer
  }

  getInitialAnswers(){
    return 0
  }
}

export class MultipleOptionsAnswers extends Answers {
  answers: OptionAnswer[]
  rightAnswers: OptionAnswer[]

  get answersTypeName(): string {
    return 'Výběr odpovědí s více správnými možnostmi';
  }


  constructor(answers: OptionAnswer[], rightAnswers: OptionAnswer[]) {
    super()
    this.answers = answers
    this.rightAnswers = rightAnswers
  }

  getInitialAnswers() {
    return []
  }
}

export class TextAreaAnswer extends Answers {
  righAnswer: string

  get answersTypeName(): string {
    return 'Textová odpověď';
  }

  constructor(righAnswer: string) {
    super()
    this.righAnswer = righAnswer
  }

  getInitialAnswers(){
    return 0
  }
}

export type TestQuestion = {
  id: number
  content: string
  answers: Answers
}
