import { VuexModule } from 'vuex-module-decorators'
import { AlreadyExistsError } from '~/types/errors'

export abstract class GeneralStore<T> extends VuexModule {
  public items = new Map<number, T>()

  get itemsAsArray(): T[] {
    return Array.from(this.items.values())
  }

  /**
   *
   * @param key
   * @param value
   * @throws AlreadyExistsError
   */
  public add(key: number, value: T) {
    if (this.items.get(key)) throw new AlreadyExistsError()
    this.items.set(key, value)
    // this.items = new Map<number, T>(this.items)
  }
}
