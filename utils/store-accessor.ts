import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'

// modules
import TestsStore from '~/store/testsStore'
import QuestionGroups from '~/store/questionGroupsStore'
import TestTemplateStore from '~/store/testTemplateStore'

// create singletons
let testsStore: TestsStore
let questionGroupsStore: QuestionGroups
let testTemplateStore: TestTemplateStore

export function initialiseStores(store: Store<any>): void {
  testsStore = getModule(TestsStore, store)
  questionGroupsStore = getModule(QuestionGroups, store)
  testTemplateStore = getModule(TestTemplateStore, store)
}

export { testsStore, questionGroupsStore, testTemplateStore }
