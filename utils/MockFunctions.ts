import { OneOptionAnswers, OptionAnswer, Test, TestQuestion } from '~/types/Test'
import { TestTemplate } from '~/types'

export const answers = [
  ['Praha', 'Brno', 'Olomouc'],
  ['x = 1', 'x = 3', 'x = 10', 'x = 20'],
  ['Option 1', 'Option 2'],
]

export class MockFunctions {
  static mockTests(): Map<number, Test> {
    return new Map([
      [
        2,
        {
          name: 'New test',
          id: 2,
          startDate: new Date(),
          duration: 20,
        },
      ],
    ])
  }

  static mockOptionAnswers(): OptionAnswer[] {
    const optionsIndex = Math.floor(Math.random() * answers.length)
    return answers[optionsIndex].map((answer, index) => ({
      key: index,
      value: answer,
    }))
  }

  static mockQuestions():TestQuestion[] {
    return [
      {
        id: 1,
        content: "Jaké je hlavní město Slovenska?",
        answers: new OneOptionAnswers([
          {
            key: 2,
            value: "Praha"
          },
          {
            key: 2,
            value: "Bratislava"
          },
          {
            key: 3,
            value: "Berlín"
          }
        ], 2)
      },
      {
        id: 2,
        content: "Jaké je hlavní město Slovenska?",
        answers: new OneOptionAnswers([
          {
            key: 2,
            value: "Praha"
          },
          {
            key: 2,
            value: "Bratislava"
          },
          {
            key: 3,
            value: "Berlín"
          }
        ], 2)
      }
    ]
  }
}
