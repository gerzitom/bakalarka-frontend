import { MultipleOptionsAnswers, OneOptionAnswers, Test, TestQuestion, TextAreaAnswer } from '~/types/Test'
import { Action, Module } from 'vuex-module-decorators'
import { MockFunctions } from '~/utils/MockFunctions'
import { GeneralStore } from '~/GeneralStore'

@Module({
  name: 'testsStore',
  namespaced: true,
  stateFactory: true,
})
export default class TestsStore extends GeneralStore<Test> {
  items = MockFunctions.mockTests()

  @Action
  async getTestQuestions(test: number): Promise<TestQuestion[]>{
    return [
      {
        id: 1,
        content: 'Toto je zadaní otázky 1',
        answers: new OneOptionAnswers(MockFunctions.mockOptionAnswers(), 1),
      },
      {
        id: 2,
        content: 'Toto je zadaní otázky 2',
        answers: new MultipleOptionsAnswers(MockFunctions.mockOptionAnswers(), []),
      },
      {
        id: 3,
        content: 'Toto je zadaní otázky 3',
        answers: new OneOptionAnswers(MockFunctions.mockOptionAnswers(), 0),
      },
      {
        id: 4,
        content: 'Toto je zadaní textove otazky 4',
        answers: new TextAreaAnswer( "Praha"),
      }
    ]
  }
}
