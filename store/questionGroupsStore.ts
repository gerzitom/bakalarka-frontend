import { Module, Mutation } from 'vuex-module-decorators'
import { QuestionGroup } from '~/types/QuestionGroup'
import { GeneralStore } from '~/GeneralStore'

@Module({
  name: 'questionGroupsStore',
  namespaced: true,
  stateFactory: true,
})
export default class QuestionGroupsStore extends GeneralStore<QuestionGroup[]> {

  @Mutation
  addQuestionGroup(group: QuestionGroup) {
    // super.add(1, group)
  }
}
