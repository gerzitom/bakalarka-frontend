import { Action, Module, Mutation } from 'vuex-module-decorators'
import { GeneralStore } from '~/GeneralStore'
import { TestTemplate } from '~/types'
import { MockFunctions } from '~/utils/MockFunctions'
import { $axios } from '~/utils/api'

@Module({
  name: 'testTemplateStore',
  namespaced: true,
  stateFactory: true,
})
export default class TestTemplateStore extends GeneralStore<TestTemplate> {

  @Mutation
  public setItems(items: TestTemplate[]){
    this.items = new Map<number, TestTemplate>()
    items.forEach(item => this.items.set(item.id, item))
  }

  @Action({ rawError: true, commit: 'setItems' })
  async loadTemplates() {
    const response = await $axios.get<TestTemplate[]>("/teachers/1/test_templates")
    return response.data
  }

  @Action({ rawError: true })
  async addTestTemplate(testTemplate: TestTemplate) {
    // TODO add API call
    const newId = 2
    // super.add(newId, testTemplate)
    return newId
  }
}
