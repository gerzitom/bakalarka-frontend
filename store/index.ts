import Vuex, { Store } from 'vuex'
// import { Store } from 'vuex'
import { initialiseStores } from '~/utils/store-accessor'
import TestsStore from '~/store/testsStore'
import TestTemplateStore from '~/store/testTemplateStore'
import QuestionGroupsStore from '~/store/questionGroupsStore'

export const store = new Vuex.Store({
  modules: {
    testsStore: TestsStore,
    questionGroupsStore: QuestionGroupsStore,
    testTemplateStore: TestTemplateStore,
  },
  /*
  Ideally if all your modules are dynamic
  then your store is registered initially
  as a completely empty object
  */
})

const initializer = (store: Store<any>) => initialiseStores(store)
export const plugins = [initializer]
export * from '~/utils/store-accessor'
